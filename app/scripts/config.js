require.config({
  paths: {
    'bower_components': '../../bower_components',
    'jquery': '../../bower_components/jquery/dist/jquery'
  },
  map: {
    '*': {
      //'jquery': 'bower_components/jquery/dist/jquery.min',
      'knockout': 'bower_components/knockout.js/knockout',
      'ko': 'bower_components/knockout.js/knockout'
    }
  }
});

// Use the debug version of knockout if development only
require.config({
  map: {
    '*': {
      'knockout': 'bower_components/knockout/dist/knockout.debug',
      'ko': 'bower_components/knockout/dist/knockout.debug'
    }
  }
});

// Entry point
require(['app'], function(){ });
