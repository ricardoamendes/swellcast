Swell Cast
==========

A web app built with Knockout.js. SwellCast features a list of surf spots by region and gives access to a detailed view containing general surf forecast info such as swell, period, wind, pressure or sea temperature.
